package it.popolano.ricettario.repository;

import it.popolano.ricettario.model.User;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@EnableAutoConfiguration
public interface UserRepository extends JpaRepository<User, Long> {

   User getUserById(Long id);
   @Transactional
   void removeUserById(Long id);

}
