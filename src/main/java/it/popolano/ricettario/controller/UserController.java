package it.popolano.ricettario.controller;

import it.popolano.ricettario.model.User;
import it.popolano.ricettario.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    //new changes
    @Autowired
    UserService userService;

    // Get USER by ID
    @GetMapping(value = "/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity(userService.getUserById(id), HttpStatus.OK);
    }

    // Get All users
    @GetMapping( value = "/users" )
    public ResponseEntity<List<User>> findAllUsers() {
        return new ResponseEntity(userService.findAllUsers(), HttpStatus.OK);
    }
    // Create a USER
    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> saveUser(@RequestBody(required = true) User user) {
        User createdUser = userService.saveUser(user);
        if(createdUser != null ) {
           return new ResponseEntity(createdUser, HttpStatus.CREATED);
        }
        return new ResponseEntity(null, HttpStatus.BAD_REQUEST);
    }

    // Delete a User
    @DeleteMapping(value = "/users/{id}")
    public ResponseEntity deleteUser(@PathVariable(value = "id") Long id) {
        userService.removeUserById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
