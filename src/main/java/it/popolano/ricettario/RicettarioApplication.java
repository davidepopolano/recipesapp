package it.popolano.ricettario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
//TODO activate annotations
//@EnableWebMvc
//@EnableJpaAuditing
public class RicettarioApplication {
	public static void main(String[] args) {
		SpringApplication.run(RicettarioApplication.class, args);
	}

}
